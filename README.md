INTRODUCTION
------------

Morningside Translator is used to translate content using Morningside
Translations ( https://www.morningtrans.com/ ).

REQUIREMENTS
------------

This module requires the following modules for better performance:

  1. Translation Management Tool
  ( https://www.drupal.org/project/tmgmt )
  2. Date ( https://www.drupal.org/project/date )
  3. Export / Import File (TMGMT submodule)


RECOMMENDED MODULES
-------------------

  1. HTMLMail ( https://www.drupal.org/project/htmlmail )


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


CONFIGURATION
-------------

From the configuration page, please provide the:
  * Customer ID
  * Contact Email ID

Users can also add additional email IDs for notification.

On successful authentication, the configuration form will show a language
mapping section where users can map site languages with Plunet languages.


TROUBLESHOOTING
---------------

Morningside translator issues will appear in the Drupal logs. To view these
logs, navigate to Administration >> Reports >> Recent log messages.


FAQ
---

1. Why do some language combination show 'translator unsupported'?
Answer: This happens because either the language combination selected for the
job are not supported by Morningside, or there is an error with the language
mapping.

To check this, please navigate to Administration >> Configuration >>
Regional and language >> Translation Management Translators


MAINTAINERS
-----------
* Baskaran B (gurubaskar) - (https://www.drupal.org/user/1470562)
