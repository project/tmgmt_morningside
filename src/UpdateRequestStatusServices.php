<?php

namespace Drupal\tmgmt_morningside;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Morningside translator UI.
 */
class UpdateRequestStatusServices {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct database connection.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */

  /**
   * Creates an UpdateRequestStatusServices object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The Database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager
    ) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateRequestStatus() {

    $translator = $this->getTranslators();
    $connection = $this->database;
    $result = $connection->select('tmgmt_morningside_requests', 'mr')
      ->fields('mr', ['requestno', 'requeststatus', 'requesttype'])
      ->execute()
      ->fetchAll();
    foreach ($result as $request) {

      switch ($request->requeststatus) {
        case 'Quote Pending':
        case 'Order Pending':
        case 'In Preparation':
        case 'Pending':
        case 'New (auto)':
        case 'Changed into quote':
        case 'Quote in Progress':

          $translator->getPlugin()
            ->updateRequest($translator, $request->requestno, $request->requesttype);
          break;

        case 'Rejected':
        case 'Changed into order':
        case 'Canceled':
        default:
      }
    }
  }

  /**
   * This function called from CRON to update quote status.
   */
  public function updateQuoteStatus() {
    $translator = $this->getTranslators();
    $connection = $this->database;
    $result = $connection->select('tmgmt_morningside_quotes', 'mq')
      ->fields('mq', ['quoteno', 'quotestatus'])
      ->execute()
      ->fetchAll();

    foreach ($result as $quote) {

      switch ($quote->quotestatus) {
        case 'Quote in Progress':
        case 'New':
        case 'Quote Complete':
        case 'Accepted':
        case 'Quote Approved':
        case 'Rejected':
          $translator->getPlugin()->updateQuote($translator, $quote->quoteno);
          break;

        case 'Quote Revised';
        case 'Quote Canceled':
        case 'Check Clearance':
        case 'Quote Expired':
        default:
      }
    }
  }

  /**
   * This function called from CRON to update order status.
   */
  public function updateOrderStatus() {
    $translator = $this->getTranslators();
    $connection = $this->database;
    $result = $connection->select('tmgmt_morningside_orders', 'mo')
      ->fields('mo', ['orderno', 'orderstatus', 'requestid'])
      ->execute()
      ->fetchAll();
    foreach ($result as $order) {
      switch ($order->orderstatus) {
        case "Quote Moved to Order":
        case "Order in Progress":
        case "Active":
          $translator->getPlugin()->updateOrder($translator, $order->orderno);
          break;

        case "Order Complete":
          $orderNo = $order->orderno;
          $requestID = $order->requestid;
          $fileList = $translator->getPlugin()->getFileList($translator, $orderNo);
          if ($fileList) {
            foreach ($fileList as $file) {
              $fileName = str_replace("\\\\", "\\", $file);
              $translator->getPlugin()->downloadFile(
                $translator, $orderNo, $requestID, $fileName
              );
            }
          }
          break;

        case "Archived":
        case "Archivable":
        default:
      }
    }
  }

  /**
   * This function is used for get translator.
   */
  public function getTranslators() {
    $translators = $this->entityTypeManager
      ->getStorage('tmgmt_translator')
      ->loadByProperties(['plugin' => 'morningside']);

    if (!$translators) {
      return;
    }

    foreach ($translators as $translator) {
      // Receive all processed translations.
      return $translator;
    }
  }

}
