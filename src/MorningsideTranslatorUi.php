<?php

namespace Drupal\tmgmt_morningside;

use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\JobInterface;

/**
 * Morningside translator UI.
 */
class MorningsideTranslatorUi extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    if ($translator->getSetting('tmgmt_morningside_API_URL') != "") {
      $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    }
    else {
      $apiUrl = \Drupal::state()->get('tmgmt_morningside_API_URL');
    }

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customer ID'),
      '#default_value' => $translator->getSetting('username'),
      '#required' => TRUE,
      '#description' => $this->t('Enter customer ID provided by Morningside.'),
    ];
    $form['contact_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customer contact email'),
      '#default_value' => $translator->getSetting('contact_email'),
      '#required' => TRUE,
      '#description' => $this->t('Enter customer contact email provided to Morningside.'),
    ];
    $form['notification_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional Notification email'),
      '#default_value' => $translator->getSetting('notification_email'),
      '#description' => $this->t('Multiple email addresses may be added, separated by comma'),
    ];
    $form['tmgmt_morningside_API_URL'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Morningside Connection URL'),
      '#default_value' => $apiUrl,
      '#required' => TRUE,
      '#description' => $this->t('Note: Do not change this without instructions of Morningside Representative'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */

    $translator = $form_state->getFormObject()->getEntity();

    $token = $translator->getPlugin()->getToken($translator);
    if (empty($token)) {
      $form_state->setErrorByName('settings][username', $this->t('The "Customer ID", the "Customer contact email" or both are not correct.'));
      $form_state->setErrorByName('settings][contact_email', $this->t('The "Customer ID", the "Customer contact email" or both are not correct.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $form['checkout_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Project Instructions'),
      '#default_value' => $job->getSetting('checkout_desc'),
      '#description' => $this->t('Enter any project instructions as applicable.'),
    ];
    $form['type_options'] = [
      '#type' => 'value',
      '#value' => [
        'Quote' => $this->t('Quote'),
        'Order' => $this->t('Order'),
      ],
    ];
    $form['request_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Request Type'),
      '#default_value' => $job->getSetting('request_type'),
      '#description' => $this->t('Select request type.'),
      '#options' => $form['type_options']['#value'],
    ];
    $form['request_due_date'] = [
      '#type' => 'date',
      '#date_format' => 'Y-m-d',
      '#title' => $this->t('Request Due Date'),
      '#date_label_position' => 'none',
      '#default_value' => $job->getSetting('request_due_date'),
    ];
    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

}
