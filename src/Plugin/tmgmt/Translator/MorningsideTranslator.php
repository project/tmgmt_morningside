<?php

namespace Drupal\tmgmt_morningside\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\JobInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AesEncryption\AesEncryption;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Morningside translator plugin.
 *
 * Check @link http://msdn.microsoft.com/en-us/library/dd576287.aspx Microsoft
 * Translator @endlink. Note that we are using HTTP API.
 *
 * @TranslatorPlugin(
 *   id = "morningside",
 *   label = @Translation("Morningside"),
 *   description = @Translation("Morningside Translator service."),
 *   ui = "Drupal\tmgmt_morningside\MorningsideTranslatorUi"
 * )
 */
class MorningsideTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The tempstore object.
   *
   * @var \Drupal\user\SharedTempStore
   */
  protected $tempStore;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Constructs a LocalActionBase object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle HTTP client.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The Database connection.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(
    ClientInterface $client,
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    Connection $database,
    FileSystemInterface $file_system,
    PrivateTempStoreFactory $temp_store_factory,
    MailManagerInterface $mail_manager
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
    $this->database = $database;
    $this->fileSystem = $file_system;
    $this->tempStore = $temp_store_factory->get('tmgmt_morningside');
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    return new static(
      $container->get('http_client'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('file_system'),
      $container->get('tempstore.private'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {

    $translator = $job->getTranslator();
    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    $jobLabel = $job->Label();
    $username = $translator->getSetting('username');
    $contact_email = $translator->getSetting('contact_email');
    $representative_email = $this->fetchAccountManagerEmail($translator);
    $cc = $translator->getSetting('notification_email');
    $sourceRemote = $job->getSourceLangcode();
    $targetRemote = $job->getTargetLangcode();
    $source_language = $this->fetchLanguageFromPlunet($translator, $sourceRemote);
    $target_language = $this->fetchLanguageFromPlunet($translator, $targetRemote);
    $token = $this->fetchToken($translator);
    $header = [
      'Authorization' => 'Bearer ' . $token,
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];
    $body = [
      "LanguageConnectID" => (string) $username,
      "MatterName" => "Morningside_CMS_" . $job->id(),
      "JobCreatorsName" => "Drupal",
      "JobCreatorsEmail" => $contact_email,
      "EmailTo" => $representative_email,
      "JobName" => "Morningside_CMS_" . $job->id(),
      "DocumentTypes" => "HTML",
      "SourceLanguages" => $source_language,
      "TargetLanguage" => $target_language,
      "RequestType" => $job->getSetting('request_type'),
      "SpecialInstructions" => $job->getSetting('checkout_desc'),
      "RequestedDueDate" => $job->getSetting('request_due_date'),
      "DescriptionMessage" => "Description Message",
    ];

    try {
      $response = $this->client->request(
        'POST', $apiUrl . '/api/PlunetRequest/Create',
        ['headers' => $header, 'form_params' => $body]
      );
    }
    catch (BadResponseException $e) {
      $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      \Drupal::logger('tmgmt_morningside')->error(
        'Morningside translator service returned the following error while
        creating request: @message', ['@message' => $error['Message']]
      );
    }

    if (isset($response)) {
      switch ($response->getStatusCode()) {
        case 201:
          $job->submitted('Request successfully sent to Morningside.');
          $result_data = json_decode($response->getBody()->getContents(), TRUE);
          $job->submitted('Request Reference Number:' . $result_data['RequestID']);

          // Echo "<pre>"; print_r($result_data);
          $result_data['jobid'] = $job->id();
          unset($result_data['OredrIDs']);
          unset($result_data['OredrNos']);
          unset($result_data['QuoteIDs']);
          unset($result_data['QuoteNos']);
          $result_data['joblabel'] = $jobLabel;

          $connection = $this->database;
          $connection->insert('tmgmt_morningside_requests')
            ->fields($result_data)->execute();

          $name = "JobID" . $job->id() . '_' .
          $job->getSourceLangcode() . '_' . $job->getTargetLangcode();

          $export = \Drupal::service(
            'plugin.manager.tmgmt_file.format')->createInstance('html', []
          );
          $contents = htmlspecialchars_decode($export->export($job));
          $byteArray = unpack("C*", $contents);

          foreach ($byteArray as $value) {
            $newByteArray[] = $value;
          }

          $csize = strlen($contents);
          $token = $this->fetchToken($translator);
          $header = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/x-www-form-urlencoded',
          ];
          $body = [
            "RequestID" => $result_data['RequestID'] ,
            "FolderType" => 2,
            "ContentLength" => $csize,
            "FilePath" => "",
            "FileName" => $name . ".html",
            "FileContent" => $newByteArray,
          ];
          try {
            $response = $this->client->request(
              'POST', $apiUrl . '/api/ProjectFiles/UploadFile',
              ['headers' => $header, 'form_params' => $body]
            );
          }
          catch (BadResponseException $e) {
            $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
            \Drupal::logger('tmgmt_morningside')->error(
              'File not upladed. Unknown error from Morningside Translator.', []
            );
            $job->rejected(
              'File not upladed. Unknown error from Morningside Translator.'
            );
          }
          if (isset($response)) {
            switch ($response->getStatusCode()) {
              case 201:
                if ($job->getSetting('request_type') == 'Order') {
                  $mailContent = \Drupal::state()->get('Order-Requested');
                }
                else {
                  $mailContent = \Drupal::state()->get('Quote-Requested');
                }

                $representative_email = $this->fetchAccountManagerEmail($translator);
                $name = \Drupal::currentUser()->getDisplayName();

                $mailContent = str_replace("[User Name]", $name, $mailContent);
                $mailContent = str_replace(
                  "[Request Number]", $result_data['RequestID'], $mailContent
                );
                $mailContent = str_replace(
                  "[Requested Due Date]", $job->getSetting('request_due_date'),
                   $mailContent
                  );
                $mailContent = str_replace(
                  "[Source Language]", $source_language, $mailContent
                );
                $mailContent = str_replace(
                  "[Target Language(s)]", $target_language, $mailContent
                );

                $module = 'tmgmt_morningside';
                $key = 'request_create';
                $to = $translator->getSetting('contact_email');
                $langcode = \Drupal::currentUser()->getPreferredLangcode();
                $params['from'] = "";
                $params['message'] = $mailContent;
                $request_type = $job->getSetting('request_type');
                $params['subject'] = $this->t('Morningside Plugin : @label Requested', ['@label' => $request_type]);
                $params['headers'] = [
                  'Cc' => $cc . ',' . $representative_email,
                ];
                $langcode = \Drupal::currentUser()->getPreferredLangcode();
                $send = TRUE;
                $result = $this->mailManager->doMail(
                  $module, $key, $to, $langcode, $params, NULL, $send
                );

                if ($result['result'] !== TRUE) {
                  \Drupal::messenger()->addMessage(
                    $this->t('There was a problem sending your message and it
                    was not sent.'), 'error'
                  );
                }
                break;

              case 400:
                $job->rejected('File Upload is rejected by Morningside Translator.');
                break;

              default:
                $job->rejected(
                  'File not upladed. Unknown error from Morningside Translator.'
                );
                break;
            }
          }
          break;

        case 400:
          \Drupal::logger('tmgmt_morningside')->error(
            'Create request failed : @message', [
              '@message' => $result_data['Message'],
            ], []
          );
          $job->rejected('Request rejected by Morningside Translator.');
          break;

        default:
          \Drupal::logger('tmgmt_morningside')->error(
            'Request not created. Unknown error from Morningside Translator.', []
          );
          $job->rejected(
            'Request not created. Unknown error from Morningside Translator.'
          );
          break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $languages = $this->fetchLanguageFromPlunet($translator);
    return $languages;
  }

  /**
   * Function to fetch language name from Plunet.
   */
  public function fetchLanguageFromPlunet($translator, $langCode = NULL) {

    $languages = [];
    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    if (isset($apiUrl)) {
      $token = $this->fetchToken($translator);
      $header = [
        'Authorization' => 'Bearer ' . $token,
        'Content-Type' => 'application/x-www-form-urlencoded',
      ];

      try {
        $response = $this->client->request(
          'GET', $apiUrl . '/api/PlunetLanguage/GetActiveLanguages',
          ['headers' => $header]
        );
      }
      catch (BadResponseException $e) {
        $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
        \Drupal::logger('tmgmt_morningside')->error(
          'Morningside translator service returned the following error while
          fetching languages: @message', ['@message' => $error['Message']]
        );
      }
      if (isset($response)) {
        switch ($response->getStatusCode()) {
          case 200:
            $replacement = $this->getDefaultRemoteLanguagesMappings();
            $languageList = json_decode($response->getBody()->getContents(), TRUE);
            if (is_null($langCode)) {
              foreach ($languageList as $language) {
                if (in_array($language['Code'], $replacement)) {
                  $language['Code'];
                  $languageCode = array_search($language['Code'], $replacement);
                  $languages[$languageCode] = $languageCode;
                }
                else {
                  $languageCode = $language['Code'];
                  $languages[$languageCode] = $languageCode;
                }
              }
              return $languages;
            }
            else {
              if (array_key_exists($langCode, $replacement)) {
                $langCode = $replacement[$langCode];
              }
              foreach ($languageList as $language) {
                if ($language['Code'] == $langCode) {
                  return $language['EnglishName'];
                }
              }
            }
            break;

          default:
            \Drupal::logger('tmgmt_morningside')->error(
              'Enable to fetch languages from Plunet ', []
            );
        }
      }
      return $languages;
    }
    else {
      return [];
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRemoteLanguagesMappings() {
    return [
      'aa' => 'aar',
      'af' => 'af-ZA',
      'ak' => 'aka',
      'sq' => 'sqi',
      'am' => 'am',
      'ar' => 'ara',
      'hy' => 'hy',
      'as' => 'asm',
      'az' => 'az-Latn',
      'bm' => 'bam',
      'eu' => 'eu-ES',
      'be' => 'bel',
      'bn' => 'ben',
      'bs' => 'bos',
      'bg' => 'bg-BG',
      'my' => 'mya',
      'ca' => 'cat',
      'ny' => 'nya',
      'zh-hans' => 'zh-CN',
      'zh-hant' => 'zh-TW',
      'hr' => 'hr-SH',
      'cs' => 'cs-CS',
      'da' => 'dan',
      'nl' => 'nl-NL',
      'en' => 'en-US',
      'en-gb' => 'en-US',
      'et' => 'et-EE',
      'ee' => 'ee-GH',
      'fo' => 'fao',
      'fj' => 'fj-FJ',
      'fil' => 'tgl',
      'fi' => 'fi-FI',
      'fr' => 'fr-FR',
      'ff' => 'ff-Latn-SN',
      'gl' => 'glg',
      'ka' => 'ka-GE',
      'de' => 'de-DE',
      'el' => 'el-GR',
      'kl' => 'kal',
      'gn' => 'gug',
      'gu' => 'guj',
      'ht' => 'cr-hat',
      'ha' => 'ha-Latn',
      'he' => 'he-IL',
      'hi' => 'hi-IN',
      'hu' => 'hu-HU',
      'is' => 'is-IS',
      'ig' => 'ibo',
      'id' => 'id-ID',
      'ga' => 'ga-IE',
      'it' => 'it-IT',
      'ja' => 'ja-JA',
      'jv' => 'jv-ID',
      'kn' => 'kn-IN',
      'ks' => 'kas',
      'kk' => 'kk-KZ',
      'ki' => 'ki-KE',
      'rw' => 'rw-RW',
      'rn' => 'rn-BI',
      'kg' => 'kon',
      'ko' => 'ko-KR',
      'ku' => 'kur',
      'ky' => 'ky-KG',
      'lo' => 'lao',
      'la' => 'lat',
      'lv' => 'lv-LV',
      'ln' => 'ln-CD',
      'lt' => 'lt-LT',
      'lg' => 'lg-UG',
      'mk' => 'mk-MK',
      'mg' => 'mlg',
      'ms' => 'ms-MY',
      'ml' => 'mal',
      'dv' => 'DIV',
      'mt' => 'mt-MT',
      'mr' => 'mr-IN',
      'mh' => 'mh-MH',
      'mo' => 'ro-MD',
      'mn' => 'mon',
      'ne' => 'ne-NP',
      'nd' => 'nd-ZW',
      'nb' => 'no-BO',
      'nn' => 'no-NO',
      'om' => 'om-ET',
      'os' => 'oss',
      'ps' => 'pus',
      'fa' => 'fas',
      'pl' => 'pl-PL',
      'pt-br' => 'pt-BR',
      'pt-pt' => 'pt-PT',
      'pa' => 'pan',
      'qu' => 'quz',
      'ro' => 'ro-RO',
      'ru' => 'ru-RU',
      'sm' => 'smo',
      'sa' => 'sa-IN',
      'sr' => 'sh-Latn',
      'sh' => 'sh-Latn',
      'sn' => 'sn-ZW',
      'sd' => 'sd-PK',
      'si' => 'si-LK',
      'sk' => 'sk-SK',
      'sl' => 'sl-SL',
      'so' => 'som',
      'nr' => 'nr-ZA',
      'es' => 'es-LA',
      'su' => 'su-ID',
      'sw' => 'swa',
      'sv' => 'sv-SE',
      'gsw-berne' => 'de-CH',
      'tl' => 'tgl',
      'ty' => 'ty-PF',
      'tg' => 'tg-Cyrl',
      'ta' => 'ta-IN',
      'tt' => 'tt-RU',
      'te' => 'te-IN',
      'th' => 'th-TH',
      'bo' => 'bo-CN',
      'ti' => 'ti-ET',
      'to' => 'ton',
      'ts' => 'ts-ZA',
      'tr' => 'tr-TR',
      'tk' => 'tk-TM',
      'tw' => 'tw-GH',
      'uk' => 'uk-UA',
      'ur' => 'ur-IN',
      'ug' => 'ug-CN',
      'uz' => 'uz-Latn-UZ',
      've' => 've-ZA',
      'vi' => 'vi-VI',
      'cy' => 'cy-GB',
      'wo' => 'wo-SN',
      'xh' => 'xh-ZA',
      'yi' => 'yid',
      'yo' => 'yo-NG',
      'zu' => 'zu',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTargetLanguages(
    TranslatorInterface $translator,
    $source_language) {
    $remote_languages = $this->getSupportedRemoteLanguages($translator);

    // There are no language pairs, any supported language can be translated
    // into the others. If the source language is part of the languages,
    // then return them all, just remove the source language.
    if (array_key_exists($source_language, $remote_languages)) {
      unset($remote_languages[$source_language]);
      return $remote_languages;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasCheckoutSettings(JobInterface $job) {
    return TRUE;
  }

  /**
   * Get the access token.
   *
   * @param \Drupal\tmgmt\Entity\TranslatorInterface $translator
   *   The translator entity to get the settings from.
   *
   * @return string
   *   The access token.
   *
   * @throws \Drupal\tmgmt\TMGMTException
   *   Thrown when the client id or secret are missing or are not valid.
   */
  public function getToken(TranslatorInterface $translator) {

    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');

    $data = \Drupal::state()->get('tmgmt_morningside_encrypt_data');

    $password = \Drupal::state()->get('tmgmt_morningside_encrypt_password');

    $client_id = \Drupal::state()->get('tmgmt_morningside_client_id');

    $aes = new AesEncryption();

    $client_secret = $aes->encrypt($data, $password);

    // Prepare Guzzle Object.
    $body = [
      'username' => $translator->getSetting('username'),
      'password' => $translator->getSetting('contact_email'),
      'grant_type' => 'password',
      'client_id' => $client_id,
      'client_secret' => trim($client_secret),
    ];

    try {
      $response = $this->client->request(
        'POST', $apiUrl . '/token', ['form_params' => $body]
      );
    }
    catch (BadResponseException $e) {
      $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      \Drupal::logger('tmgmt_morningside')->error(
        'Morningside translator service returned the following error while
        generating token: @message', ['@message' => $error['error_description']]
      );
    }
    if (isset($response)) {
      if ($response->getStatusCode() == 200) {
        $data = json_decode($response->getBody()->getContents(), TRUE);
        $data['createTimeStamp'] = time();
        $this->tempStore->set('Web_API_data', $data);
        // Echo "<pre>"; print_r($data);
        return TRUE;
      }
      else {
        \Drupal::logger('tmgmt_morningside')->error('Failed to acquire token: ', []);
        return FALSE;
      }
    }
    else {
      \Drupal::logger('tmgmt_morningside')->error('Failed to acquire token: ', []);
      return FALSE;
    }
  }

  /**
   * Function for fetch Token.
   */
  public function fetchToken($translator) {
    $web_API_data = $this->tempStore->get('Web_API_data');
    if (isset($web_API_data)) {
      $creationTime = $web_API_data['createTimeStamp'];
      $currentTime = time();
      $timeDiff = $currentTime - $creationTime;
      $expireIn = $web_API_data['as:access_token_expires_in'];
      $expireIn = (($expireIn - 1) * 60);

      if ($timeDiff > $expireIn) {
        // Refresh Token.
        $refreshTokenExpiry = $web_API_data['as:refresh_token_expires_in'];
        $refreshTokenExpiry = (($refreshTokenExpiry - 1) * 60);

        if ($timeDiff > $refreshTokenExpiry) {
          $this->getToken($translator);
          $web_API_data = $this->tempStore->get('Web_API_data');
          return $web_API_data['access_token'];
        }
        else {

          $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');

          $data = \Drupal::state()->get('tmgmt_morningside_encrypt_data');

          $password = \Drupal::state()->get('tmgmt_morningside_encrypt_password');

          $client_id = \Drupal::state()->get('tmgmt_morningside_client_id');

          $aes = new AesEncryption();

          $client_secret = $aes->encrypt($data, $password);

          $header = [
            'Content-Type' => 'application/x-www-form-urlencoded',
          ];

          $body = [
            'refresh_token' => $web_API_data['refresh_token'],
            'grant_type' => 'refresh_token',
            'client_id' => $client_id,
            'client_secret' => trim($client_secret),
          ];

          try {
            $response = $this->client->request(
              'POST', $apiUrl . '/token', [
                'headers' => $header,
                'form_params' => $body,
              ]
            );
          }
          catch (BadResponseException $e) {
            $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
            \Drupal::logger('tmgmt_morningside')->error(
              'Morningside translator service returned the following error
              while generating token using refresh token: @message', [
                '@message' => $error['error_description'],
              ]
            );
          }
          if (isset($response)) {
            if ($response->getStatusCode() == 200) {
              $data = json_decode($response->getBody()->getContents(), TRUE);
              $data['createTimeStamp'] = time();
              $this->tempStore->set('Web_API_data', $data);
              return $data['access_token'];
            }
            else {
              $this->tempStore->set('Web_API_data', "");
              $this->getToken($translator);
              $web_API_data = $this->tempStore->get('Web_API_data');
              if (isset($web_API_data)) {
                return $web_API_data['access_token'];
              }
            }
          }
          else {
            $this->tempStore->set('Web_API_data', "");
            $this->getToken($translator);
            $web_API_data = $this->tempStore->get('Web_API_data');
            if (isset($web_API_data)) {
              return $web_API_data['access_token'];
            }
          }
        }
      }
      else {
        return $web_API_data['access_token'];
      }
    }
    else {
      $this->getToken($translator);
      $web_API_data = $this->tempStore->get('Web_API_data');
      if (isset($web_API_data)) {
        return $web_API_data['access_token'];
      }
    }
  }

  /**
   * Updated request status.
   */
  public function updateRequest($translator, $requestNo, $requestType) {
    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    $token = $this->fetchToken($translator);
    $header = [
      'Authorization' => 'Bearer ' . $token,
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];

    try {
      $response = $this->client->request(
        'GET', $apiUrl . '/api/PlunetRequest/GetStatus?requestNo='
        . $requestNo, ['headers' => $header]
      );
    }
    catch (BadResponseException $e) {
      $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      \Drupal::logger('tmgmt_morningside')->error(
        'Morningside translator service returned the following error while
        fetching request: @message', ['@message' => $error['Message']]
      );
    }

    if (isset($response)) {
      switch ($response->getStatusCode()) {
        case 200:
          $requestData = json_decode($response->getBody()->getContents(), TRUE);

          $connection = $this->database;
          $connection->update('tmgmt_morningside_requests')
            ->fields(['requeststatus' => $requestData['RequestStatus']])
            ->condition('requestno', $requestNo, '=')
            ->execute();
          if ($requestData['QuoteNos']) {
            $quoteNos = explode(",", $requestData['QuoteNos']);
            foreach ($quoteNos as $quoteNo) {
              $result = $connection->select('tmgmt_morningside_quotes', 'mr')
                ->fields('mr', ['requestno'])
                ->condition('quoteno', $quoteNo, '=')
                ->condition('requestno', $requestNo, '=')
                ->execute()
                ->fetchAll();
              if (empty($result)) {
                $data = [
                  'quoteno' => $quoteNo,
                  'requestno' => $requestNo,
                  'quotestatus' => 'Quote in Progress',
                ];
                $connection->insert('tmgmt_morningside_quotes')
                  ->fields($data)->execute();
              }
            }
          }
          if ($requestType == 'Order') {
            if ($requestData['OredrNos']) {
              $orderNos = explode(",", $requestData['OredrNos']);
              foreach ($orderNos as $orderNo) {
                $data = [
                  'orderno' => $orderNo,
                  'orderstatus' => 'Order in Progress',
                  'requestid' => $requestData['RequestID'],
                ];
                $connection->insert('tmgmt_morningside_orders')
                  ->fields($data)->execute();
              }
            }
          }
          break;

        default:
          \Drupal::logger('tmgmt_morningside')
            ->error('Enable to fetch latested status of requests', []);
      }
    }
  }

  /**
   * Function for update quote using cron.
   */
  public function updateQuote($translator, $quoteNo) {
    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    $token = $this->fetchToken($translator);
    $header = [
      'Authorization' => 'Bearer ' . $token,
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];

    try {
      $response = $this->client->request(
        'GET', $apiUrl . '/api/PlunetQuote/GetQuote?quoteNo='
        . $quoteNo, ['headers' => $header]
      );
    }
    catch (BadResponseException $e) {
      $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      \Drupal::logger('tmgmt_morningside')->error(
        'Morningside translator service returned the following error while
        fetching quote: @message', ['@message' => $error['Message']]
      );
    }

    if (isset($response)) {
      switch ($response->getStatusCode()) {
        case 200:
          $requestData = json_decode($response->getBody()->getContents(), TRUE);
          $data = [
            'quoteid' => $requestData['QuoteID'],
            'rate' => $requestData['Rate'],
            'currency' => $requestData['Currency'],
            'estimatedprice' => $requestData['EstimatedPrice'],
            'turnaroundtime' => $requestData['TurnaroundTime'],
            'services' => $requestData['Services'],
            'projectnotes' => $requestData['ProjectNotes'],
            'quotestatus' => $requestData['QuoteStatus'],
          ];
          $connection = $this->database;
          $connection->update('tmgmt_morningside_quotes')
            ->fields($data)
            ->condition('quoteno', $quoteNo, '=')
            ->execute();

          if ($requestData['OrderNo']) {
            $orderNos = explode(",", $requestData['OrderNo']);
            foreach ($orderNos as $orderNo) {
              $data = [
                'orderno' => $orderNo,
                'quoteno' => $quoteNo,
                'orderstatus' => $requestData['QuoteStatus'],
                'requestid' => $requestData['RequestID'],

              ];
              $connection->insert('tmgmt_morningside_orders')
                ->fields($data)->execute();
              $connection->update('tmgmt_morningside_quotes')
                ->fields(['quotestatus' => 'Changed into order'])
                ->condition('quoteno', $quoteNo, '=')
                ->execute();
              $connection->update('tmgmt_morningside_requests')
                ->fields(['requeststatus' => 'Changed into order'])
                ->condition('requestid', $requestData['RequestID'], '=')
                ->execute();
            }
          }
          break;

        default:
          \Drupal::logger('tmgmt_morningside')
            ->error('Enable to fetch latested status of quotes', []);
      }
    }
  }

  /**
   * Function for update order using cron.
   */
  public function updateOrder($translator, $orderNo) {
    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    $token = $this->fetchToken($translator);
    $header = [
      'Authorization' => 'Bearer ' . $token,
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];
    try {
      $response = $this->client->request(
        'GET', $apiUrl . '/api/PlunetOrder/GetOrder?orderNo='
        . $orderNo, ['headers' => $header]
      );
    }
    catch (BadResponseException $e) {
      $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      \Drupal::logger('tmgmt_morningside')->error(
        'Morningside translator service returned the following error while
        fetching order: @message', ['@message' => $error['Message']]
      );
    }

    if (isset($response)) {
      switch ($response->getStatusCode()) {
        case 200:
          $requestData = json_decode($response->getBody()->getContents(), TRUE);
          $date = strtotime($requestData['DeliveryDate']);
          $data = [
            'orderid' => $requestData['OrderID'],
            'invoiceamount' => $requestData['InvoiceAmount'],
            'deliverydate' => date('Y-m-d H:i:s', $date),
            'orderstatus' => $requestData['OrderStatus'],
          ];
          $connection = $this->database;
          $connection->update('tmgmt_morningside_orders')
            ->fields($data)
            ->condition('orderno', $orderNo, '=')
            ->execute();
          break;

        default:
          \Drupal::logger('tmgmt_morningside')->error(
            'Enable to fetch latested status of orders', []
          );
      }
    }
  }

  /**
   * Function for Fetch file list from order number.
   */
  public function getFileList($translator, $orderNo) {
    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    $token = $this->fetchToken($translator);

    $header = [
      'Authorization' => 'Bearer ' . $token,
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];
    try {
      $response = $this->client->request(
        'GET', $apiUrl . '/api/ProjectFiles/GetFileList?folderType=12&orderNo='
        . $orderNo, ['headers' => $header]
      );
    }
    catch (BadResponseException $e) {
      $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      \Drupal::logger('tmgmt_morningside')->error(
        'Morningside translator service returned the following error while
        fetching file list: @message', ['@message' => $error['Message']]
      );
    }

    if (isset($response)) {
      switch ($response->getStatusCode()) {
        case 200:
          $return = json_decode($response->getBody()->getContents(), TRUE);
          break;

        default:
          \Drupal::logger('tmgmt_morningside')->error('Enable to fetch file.', []);
          $return = FALSE;
          break;
      }
    }
    return $return;
  }

  /**
   * Function for Fetch file list from order number.
   */
  public function fetchAccountManagerEmail($translator) {
    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    $username = $translator->getSetting('username');
    $token = $this->fetchToken($translator);

    $header = [
      'Authorization' => 'Bearer ' . $token,
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];
    try {
      $response = $this->client->request(
        'GET', $apiUrl . '/api/PlunetUser/GetPlunetUser?LanguageConnectID='
        . $username, ['headers' => $header]
      );
    }
    catch (BadResponseException $e) {
      \Drupal::logger('tmgmt_morningside')->error(
        "Enable to fetch account manager's Email", []
      );
    }

    if (isset($response)) {
      switch ($response->getStatusCode()) {
        case 200:
          $data = json_decode($response->getBody()->getContents(), TRUE);
          $managerEmail = $data['AccountManagersEmail'];
          break;

        default:
          \Drupal::logger('tmgmt_morningside')->error('Enable to fetch file.', []);
          break;
      }
    }
    return $managerEmail;
  }

  /**
   * Function for download file.
   */
  public function downloadFile($translator, $orderNo, $requestId, $file) {

    $apiUrl = $translator->getSetting('tmgmt_morningside_API_URL');
    $token = $this->fetchToken($translator);

    $header = [
      'Authorization' => 'Bearer ' . $token,
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];

    $downloadUrl = 'orderNo=' . $orderNo . '&folderType=12&filePathName=' . $file;

    try {
      $response = $this->client->request(
        'GET', $apiUrl . '/api/ProjectFiles/DownloadFile?'
        . $downloadUrl, ['headers' => $header]
      );
    }
    catch (BadResponseException $e) {
      $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      \Drupal::logger('tmgmt_morningside')->error(
        'Morningside translator service returned the following error while
        fetching file: @message', ['@message' => $error['Message']]
      );
    }

    $returnFile = FALSE;

    if (isset($response)) {
      switch ($response->getStatusCode()) {
        case 200:
          $requestData = json_decode($response->getBody()->getContents(), TRUE);
          $text = base64_decode($requestData['FileContent']);
          $this->pushContent($translator, $text, $requestId, $orderNo);
          break;

        default:
          $msg = 'Not able to download ' . $file;
          \Drupal::logger('tmgmt_morningside')->error($msg, []);
          break;
      }
    }
    return $returnFile;
  }

  /**
   * Fetch the content from downloaded file and stored in review section.
   */
  public function pushContent($translator, $file, $requestID, $orderNo) {
    $connection = $this->database;
    $result = $connection->select('tmgmt_morningside_requests', 'mr')
      ->fields('mr', ['jobid'])
      ->condition('requestid', $requestID, '=')
      ->execute()
      ->fetchObject();
    if (!empty($result->jobid)) {
      $jobs = \Drupal::entityTypeManager()
        ->getStorage('tmgmt_job')->loadByProperties(['tjid' => $result->jobid]);
      foreach ($jobs as $job) {
        // Receive all processed translations.
        $job;
      }
      if (!empty($job)) {
        try {
          $job->addTranslatedData($this->import($file));
          $job->addMessage('Successfully imported file.');
          $data = [
            'OrderStatus' => 'File Downloaded',
          ];
          $connection = $this->database;
          $connection->update('tmgmt_morningside_orders')
            ->fields($data)
            ->condition('requestid', $requestID, '=')
            ->execute();

          $mailContent = \Drupal::state()->get('Translation-Imported');
          $representative_email = $this->fetchAccountManagerEmail($translator);
          $mailContent = str_replace("[User Name]", "Customer", $mailContent);
          $mailContent = str_replace("[Order Number]", $orderNo, $mailContent);
          $mailContent = str_replace(
            "[Request Number]", $requestID, $mailContent
          );

          $module = 'tmgmt_morningside';
          $key = 'translation_imported';
          $to = $translator->getSetting('contact_email');
          $cc = $translator->getSetting('notification_email');
          $langcode = \Drupal::currentUser()->getPreferredLangcode();
          $params['from'] = "";
          $params['message'] = $mailContent;
          $params['subject'] = $this->t(
            'Morningside Plugin : Translation Imported'
          );
          $params['headers'] = [
            'Cc' => $cc . ',' . $representative_email,
          ];
          $send = TRUE;

          $result = $this->mailManager->mail(
            $module, $key, $to, $langcode, $params, NULL, $send
          );
          return TRUE;
        }
        catch (Exception $e) {
          $job->addMessage(
            'File import failed with the following message: @message',
            ['@message' => $e->getMessage()],
            'error'
          );
        }
      }
    }
    return FALSE;
  }

  /**
   * Import HTML content and return array.
   */
  public function import($imported_file, $is_file = TRUE) {

    $dom = new \DOMDocument();
    if ($dom->loadHTML($imported_file)) {
      $xpath = new \DOMXpath($dom);

      $data = [];
      foreach ($xpath->query("//div[@class='atom']") as $atom) {
        // Assets are our strings (eq fields in nodes).
        $key = $this->decodeIdSafeBase64($atom->getAttribute('id'));
        $data[$key]['#text'] = '';
        foreach ($atom->childNodes as $node) {
          $data[$key]['#text'] .= $dom->saveHTML($node);
        }
      }
    }
    else {
      return FALSE;
    }
    return \Drupal::service('tmgmt.data')->unflatten($data);
  }

  /**
   * Returns decoded id safe base64 data.
   */
  protected function decodeIdSafeBase64($data) {
    // Remove prefixed b.
    $data = substr($data, 1);
    return base64_decode(
      str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)
    );
  }

}
